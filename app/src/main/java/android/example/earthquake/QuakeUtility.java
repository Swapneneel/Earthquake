package android.example.earthquake;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper methods related to requesting and receiving earthquake data from USGS.
 */
final class QuakeUtility {

    /* The name to be used in case of logging */
    private static final String LOG_TAG = QuakeUtility.class.getSimpleName();


    /**
     * Create a private constructor because no one should ever create a {@link QuakeUtility} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QuakeUtility() {
    }


    /* *********** Proceeding for network activity *****/

    /**
     * This is the public method that other methods will call to get the json data from internet
     *
     * @param requestUrl is the String url of USGS
     * @return earthquakeList of data type List<Earthquake>
     */
    static List<Earthquake> fetchEarthquakeData(String requestUrl) {

        Log.i(LOG_TAG, "fetchEarthquakeData method is called");

        URL url = createUrl(requestUrl);

        /*
         * This option is to verify if the progress is working by, making the thread slow
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        */

        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making http request ", e);
        }

        List<Earthquake> earthquakesList = extractFeatureFromJSON(jsonResponse);

        /* returns the list of {@link Earthquake }*/
        return earthquakesList;
    }


    /**
     * Returns new URL object from the given string URL.
     */
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }


    /**
     * Makes a HTTP request
     *
     * @param url is the Url created by createUrl method
     * @return jsonResponse string
     * @throws IOException is for catching the exception can be
     */
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        /* Sanity check for the url */
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000  /* in milliseconds */);
            urlConnection.setConnectTimeout(15000 /* in milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();

                /* Convert the whole json response */
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error connection to the server " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (inputStream != null) {
                inputStream.close();
            }
        }

        return jsonResponse;
    }


    /**
     * Convert he {@link InputStream} into a String which contains the whole JSON response
     *
     * @param inputStream is the binary stream came from the http response
     * @return the entire JSON response as a string
     * @throws IOException if the string is not formatted properly
     */
    private static String readFromStream(InputStream inputStream) throws IOException {

        StringBuilder outputString = new StringBuilder();

        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String line = bufferedReader.readLine();
            while (line != null) {
                outputString.append(line);
                line = bufferedReader.readLine();
            }
        }

        return outputString.toString();
    }




    /* *********** Proceeding for JSON Parse activity *****/

    /**
     * Return a list of {@link Earthquake} objects that has been built up from
     * parsing a JSON response.
     */
    private static ArrayList<Earthquake> extractFeatureFromJSON(String earthquakeJSON) {

        // Create an empty ArrayList that we can start adding earthquakes to
        ArrayList<Earthquake> earthquakes = new ArrayList<>();

        // Try to parse the SAMPLE_JSON_RESPONSE. If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {

            // TODO: Parse the response given by the SAMPLE_JSON_RESPONSE string and
            // build up a list of Earthquake objects with the corresponding data.
            JSONObject mainObj = new JSONObject(earthquakeJSON);
            JSONArray quakeListArray = mainObj.getJSONArray("features");

            /* Looping through the number of responses */
            for (int i = 0, len = quakeListArray.length(); i < len; i++) {
                /* Element inside features */
                JSONObject oneQuake = quakeListArray.getJSONObject(i);

                /* Properties of one element */
                JSONObject properties = oneQuake.getJSONObject("properties");

                /* Values of Properties of one element */
                double mag = properties.getDouble("mag");
                String location = properties.getString("place");
                long timeLong = properties.getLong("time");
                /* Try to get the url if available */
                String infoUrl = properties.getString("url");


                /* Adding the values to the Earthquake obj */
                earthquakes.add(new Earthquake(mag, location, timeLong, infoUrl));

            }

        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(LOG_TAG, "Problem parsing the earthquake JSON results", e);
        }

        // Return the list of earthquakes
        return earthquakes;
    }

}