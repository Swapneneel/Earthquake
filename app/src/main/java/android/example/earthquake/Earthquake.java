package android.example.earthquake;


class Earthquake {

    /** States of the class Earthquake **/
    /* This will hold the magnitude */
    private double mMagnitude;

    /* Holds the place of quake */
    private String mPlaceName;

    /* Holds date of the event */
    private long mUnixDate;

    /* Hold the url for the particular json object */
    private String mInfoUrl;



    /**
     * This is the obj
     * @param mag The magnitude of the earthquake
     * @param place The place it occur
     * @param date The date of the event
     * @param url is the url of the
     */
    Earthquake(double mag, String place, long date, String url) {
        mMagnitude = mag;
        mPlaceName = place;
        mUnixDate = date;
        mInfoUrl = url;
    }



    /* Method of the obj */


    /**
     * Provides the magnitude of the earthquake
     * @return (float) magnitude
     */
    double getMagnitudeText() {
       return mMagnitude;
    }


    String getPlaceName() {
        return mPlaceName;
    }

    long getDateOfEvent() {
        return mUnixDate;
    }


    String getInfoUrl() {
        return mInfoUrl;
    }
}
