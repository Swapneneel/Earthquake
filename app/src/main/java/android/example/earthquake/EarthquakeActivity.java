package android.example.earthquake;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.content.Loader;
import android.app.LoaderManager.LoaderCallbacks;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;


public class EarthquakeActivity extends AppCompatActivity implements LoaderCallbacks<List<Earthquake>> {

    /* Base url for USGS api call */
    private static final String USGS_BASE_API_URL = "https://earthquake.usgs.gov/fdsnws/event/1/query";

    /* Name of activity to be used in Log msg*/
    private static final String LOG_TAG = EarthquakeActivity.class.getName();
    /**
     * Constant value for the earthquake loader ID. We can choose any integer.
     * This really only comes into play if you're using multiple loaders.
     */
    private static final int EARTHQUAKE_LOADER_ID = 1;
    /* Adapter for list of earthquakes */
    private EarthquakeAdapter mAdapter;

    /* Empty text view holder */
    private TextView emptyTextView;

    /* Btn in empty text view */
    private Button tryAgainBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        Log.i(LOG_TAG, "onCreate method is called");

        /* Check the network status and store in a boolean for later use */
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();


        // Get a reference to the ListView, and attach the adapter to the listView.
        ListView listView = findViewById(R.id.list);

        // Create a new adapter that takes an empty list of earthquakes as input
        mAdapter = new EarthquakeAdapter(this, new ArrayList<Earthquake>());


        /* Setting the EmptyView layout */
        LinearLayout emptyViewHolder = findViewById(R.id.empty_view);

        // empty text view
        emptyTextView = findViewById(R.id.empty_view_txt);
        // try again btn
        tryAgainBtn = findViewById(R.id.try_again_btn);

        // now empty view is a layout
        listView.setEmptyView(emptyViewHolder);

        tryAgainBtn.setVisibility(View.GONE);


        /* Set the adapter to the ListView */
        listView.setAdapter(mAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Earthquake currentQuake = mAdapter.getItem(position);

                assert currentQuake != null;
                Uri infoUri = Uri.parse(currentQuake.getInfoUrl());

                Intent intent = new Intent(Intent.ACTION_VIEW, infoUri);

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });


        /* Call LoaderManager when we're connected to INTERNET */
        if (isConnected) {
            // Get a reference to the LoaderManager, in order to interact with loaders.
            LoaderManager loaderManager = getLoaderManager();

            // Initialize the loader. Pass in the int ID constant defined above and pass in null for
            // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
            // because this activity implements the LoaderCallbacks interface).
            loaderManager.initLoader(EARTHQUAKE_LOADER_ID, null, this);
        } else {
            ProgressBar circleProgress = findViewById(R.id.progressBar);
            circleProgress.setVisibility(View.GONE);

            emptyTextView.setText(R.string.no_internet);
            tryAgainBtn.setVisibility(View.VISIBLE);

            /* Reload the page with  help of modifying SharedPreference value  */
            tryAgainBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new Intent(EarthquakeActivity.this, EarthquakeActivity.class);

                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);

                    Log.v(LOG_TAG, "Try Again clicked");
                }
            });
        }


    }

    /* These are method overrides for showing up the menu items */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsItem = new Intent(this, SettingsActivity.class);
            startActivity(settingsItem);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /* Menu option done */


    /**
     * This will call the create loader method
     *
     * @param id   quite not sure yet
     * @param args not sure
     * @return a {@link List<Earthquake> object from the internet }
     */
    @Override
    public Loader<List<Earthquake>> onCreateLoader(int id, Bundle args) {
        Log.i(LOG_TAG, "onCreateLoader activated");

        /* Make the url from the user's pref using SharedPreference */
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String minMagnitude = sharedPreferences.getString(getString(R.string.settings_min_magnitude_key), getString(R.string.settings_min_magnitude_default));

        String orderBy = sharedPreferences.getString(getString(R.string.settings_order_by_key), getString(R.string.settings_order_by_default));

        /* Time to parse and make the uri for use using the base uri */
        Uri baseUri = Uri.parse(USGS_BASE_API_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        // Adding the parameters
        uriBuilder.appendQueryParameter("format", "geojson");
        uriBuilder.appendQueryParameter("limit", "30");
        uriBuilder.appendQueryParameter("minmag", minMagnitude);
        uriBuilder.appendQueryParameter("orderby", orderBy);

        return new EarthquakeLoader(this, uriBuilder.toString());
    }


    /**
     * This method is invoked on finishing the the loading action
     *
     * @param loader      is loader obj of type List<Earthquake>
     * @param earthquakes is the list obj
     */
    @Override
    public void onLoadFinished(Loader<List<Earthquake>> loader, List<Earthquake> earthquakes) {
        mAdapter.clear();

        if (earthquakes != null && !earthquakes.isEmpty()) {
            mAdapter.addAll(earthquakes);
        }

        /* Remove the progress bare as the loading finishes */
        ProgressBar circleProgress = findViewById(R.id.progressBar);
        circleProgress.setVisibility(View.GONE);

        /* Set the text message for empty text */
        emptyTextView.setText(R.string.no_earthquakes);

        tryAgainBtn.setText(R.string.suggestion_btn_txt);
        tryAgainBtn.setVisibility(View.VISIBLE);

        // On click this btn open the settings for the user
        tryAgainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingsIntent = new Intent(EarthquakeActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
            }
        });

        Log.i(LOG_TAG, "onLoaderFinished method is called");
    }

    /**
     * @param loader is the obj of type List<Earthquake>
     */
    @Override
    public void onLoaderReset(Loader<List<Earthquake>> loader) {
        mAdapter.clear();

        Log.i(LOG_TAG, "onLoaderReset method is called");
    }
}


/*
 * Json @param
 * 1) Main obj = features
 * 2) obj items = features[0], features[1], and so on
 * 3) each_obj = features[0].properties
 * 4) Magnitude: each_obj.mag (int)
 * 5) Place = each_obj.place (Str)
 * 6) Time = each_obj.time (int: unix time stamp)
 * 7) Alert = each_obj.alert (Str, for ex: green, red, yellow)
 * 8) Title = each_obj.title (Str)
 * 9) url = each_obj.url (Str)
 */
