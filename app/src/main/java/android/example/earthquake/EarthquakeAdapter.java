package android.example.earthquake;

import android.app.Activity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.graphics.drawable.GradientDrawable;

import java.util.ArrayList;
import java.util.Date;


public class EarthquakeAdapter extends ArrayAdapter<Earthquake> {


    /**
     * Constructor of this EarthquakeAdapter
     *
     * @param context    it is the Activity name, which will call the constructor
     * @param earthquake is the obj
     */
    EarthquakeAdapter(Activity context, ArrayList<Earthquake> earthquake) {
        super(context, 0, earthquake);
    }


    /**
     * Overriding the getView() method
     *
     * @param position    current position of the earthquake obj
     * @param convertView is the listView item
     * @param parent      is the ListView obj to hold the list items
     * @return listItemView
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // check is the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.quake_item, parent, false);
        }


        // get Earthquake obj located at this position in the list
        final Earthquake currentQuake = getItem(position);



        /* Set the magnitude text in the list item */
        assert currentQuake != null;
        double magnitudeDouble = currentQuake.getMagnitudeText();
        TextView magTextView = (TextView) listItemView.findViewById(R.id.magnitude_txt);
        magTextView.setText(magnitudeFormatter(magnitudeDouble));

        /* Set the magnitude background color */
        // Fetch the background from the TextView (magnitude) which is GradientDrawable
        GradientDrawable magnitudeCircle = (GradientDrawable) magTextView.getBackground();

        // Get the color res id from the function
        int magColor = getMagnitudeColor(magnitudeDouble);

        // set the color of the background with the color res id
        magnitudeCircle.setColor(magColor);



        /* Calling the text splitter below */
        String[] placeNameLocation = splitLocationAndPlaceName(currentQuake.getPlaceName());

        /* Set the location of incident list item */
        TextView locationText = (TextView) listItemView.findViewById(R.id.location_txt);
        locationText.setText(placeNameLocation[0]);

        /* Set the place name to the list item */
        TextView placeText = (TextView) listItemView.findViewById(R.id.place_txt);
        placeText.setText(placeNameLocation[1]);

        /* Have to convert the date long to str */
        Date dateObj = new Date(currentQuake.getDateOfEvent());

        /* Set the date to the list view item */
        TextView dateView = (TextView) listItemView.findViewById(R.id.date_view);
        dateView.setText(formatDate(dateObj));

        /* Set the time to the list view item */
        TextView timeView = (TextView) listItemView.findViewById(R.id.time_view);
        timeView.setText(formatTime(dateObj));

        return listItemView;
    }


    /**
     * Converting the long date to Str date
     *
     * @param d is long date obj
     * @return str dateOnly
     */
    private String formatDate(Date d) {
        SimpleDateFormat dateOnly = new SimpleDateFormat("MMM dd, yyyy");
        return dateOnly.format(d);
    }


    /**
     * Converting the long to date obj to Str time
     *
     * @param d is long Date obj
     * @return str timeOnly
     */
    private String formatTime(Date d) {
        SimpleDateFormat timeOnly = new SimpleDateFormat("HH:mm a");
        return timeOnly.format(d);
    }


    /**
     * Split the place and offset from one string got from "place"
     *
     * @param location String value got from JSON parameter "place"
     * @return String array: offset
     */
    private String[] splitLocationAndPlaceName(String location) {
        //
        String[] offset;
        if (location.contains("of")) {
            offset = location.split("(?<=of )", 2);
        } else {
            offset = new String[]{getContext().getString(R.string.near_the), location};
        }
        return offset;
    }


    /**
     * Convert the magnitude string to, one decimal magnitude string
     *
     * @param mag magnitude is double data type
     * @return formatted magnitude string
     */
    private String magnitudeFormatter(double mag) {
        DecimalFormat formatter = new DecimalFormat("0.0");
        return formatter.format(mag);
    }


    /**
     * Fetch the color res id according to the magnitude value
     *
     * @param magnitude is the magnitude in double type
     * @return color res id
     */
    private int getMagnitudeColor(double magnitude) {

        int mag = (int) Math.floor(magnitude);
        int colorResId;
        switch (mag) {
            case 0:
            case 1:
                colorResId = R.color.magnitude1;
                break;
            case 2:
                colorResId = R.color.magnitude2;
                break;
            case 3:
                colorResId = R.color.magnitude3;
                break;
            case 4:
                colorResId = R.color.magnitude4;
                break;
            case 5:
                colorResId = R.color.magnitude5;
                break;
            case 6:
                colorResId = R.color.magnitude6;
                break;
            case 7:
                colorResId = R.color.magnitude7;
                break;
            case 8:
                colorResId = R.color.magnitude8;
                break;
            case 9:
                colorResId = R.color.magnitude9;
                break;
            default:
                colorResId = R.color.magnitude10plus;
                break;
        }

        return ContextCompat.getColor(getContext(), colorResId);
    }

}
