package android.example.earthquake;

import android.content.Context;
import android.content.AsyncTaskLoader;
import android.util.Log;

import java.util.List;


public class EarthquakeLoader extends AsyncTaskLoader<List<Earthquake>> {

    /* String tag will be used for logging */
    private static final String LOG_TAG = EarthquakeLoader.class.getName();

    /* Holds the url string to be ued to get the data */
    private String mUrl;


    /**
     * Constructs the EarthquakeLoader
     *
     * @param context is the Activity from which Earthquake loader will be called
     * @param url     is the url to make HTTP req
     */
    EarthquakeLoader(Context context, String url) {
        super(context);

        //TODO: Constructor implementation remaining
        mUrl = url;
    }


    @Override
    protected void onStartLoading() {

        Log.i(LOG_TAG, "onStartLoading method is called");

        /* This is a very good practice to call this method */
        forceLoad();
    }


    /**
     * This is on a background thread.
     */
    @Override
    public List<Earthquake> loadInBackground() {

        Log.i(LOG_TAG, "loadInBackground method is called");

        // TODO: implement this method

        // sanity check of the urls array
        if (mUrl == null) {
            return null;
        }

        /* Call the fetch EarthquakeData method to get a list of the earthquakes from the internet*/
        return QuakeUtility.fetchEarthquakeData(mUrl);

    }
}
